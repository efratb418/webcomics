# Transcript of Pepper&Carrot Episode 34 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|Episode 34: The Graduation of Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|The next day...
<unknown>|2|False|... and now, we give Shichimi the Higher Degree of the school of Ah.
<unknown>|3|False|... Still no trace of Pepper?
<unknown>|4|False|None!
<unknown>|5|False|It's a shame: she will miss Shichimi's speech
<unknown>|6|False|Thank you...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|Zio oOOOOO
<unknown>|2|False|Zioo O O O OO
<unknown>|3|False|Waaarning!
<unknown>|4|False|Step aside!
<unknown>|5|False|STEP A-SI-DE!!!
<unknown>|6|False|CR A S H!
<unknown>|7|False|Zioo O O O OO
<unknown>|8|False|Zioo O O O OO
<unknown>|9|False|Oopsie!
<unknown>|10|False|Nothing broken? Is everyone OK?
<unknown>|11|False|Pepper!
<unknown>|12|False|Hello Shichimi!
<unknown>|13|False|Sorry for my landing and late!
<unknown>|14|False|I had some problems but it's a long story!
<unknown>|15|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|Is she one of your guests?
<unknown>|2|False|Yes, she is my friend, Pepper, everything is fine.
<unknown>|3|False|Are you ok Carrot?
<unknown>|4|False|Sorry for the landing, I haven't mastered it yet while using hyper-speed.
<unknown>|5|False|...and again sorry to all for the inconvenience
<unknown>|6|False|...and for my clothing.
<unknown>|7|False|Shichimi,
<unknown>|8|False|this young witch who has just arrived... is she really a friend of yours?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|Yes your highness.
<unknown>|2|False|Her name is Pepper of the Chaosah School.
<unknown>|3|False|Her very presence here pollutes the sacred nature of our school.
<unknown>|4|False|Put her out of my sight immediately.
<unknown>|5|False|Master Wasabi...
<unknown>|6|False|But...
<unknown>|7|False|Any comment?
<unknown>|8|False|Or would you prefer to be banned from our school?
<unknown>|9|False|! ! !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|Sorry Pepper, but you have to leave.
<unknown>|2|False|Now.
<unknown>|3|False|Huh?
<unknown>|4|False|Hop hop hop! Minute! I'm sure there is a misunderstanding here.
<unknown>|5|False|Please Pepper, go away without fuss...
<unknown>|6|False|He you there on your throne, if you have a problem with me, come and tell me yourself!
<unknown>|7|False|Tsss...
<unknown>|8|False|Shichimi, you have ten seconds...
<unknown>|9|False|nine...
<unknown>|10|False|eight...
<unknown>|11|False|seven...
<unknown>|12|False|ENOUGH PEPPER, GO AWAY !!!
<unknown>|13|False|SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|B ADuuM!
<unknown>|2|False|Shichimi, please calm d...
<unknown>|3|False|GO AWAY!!!
<unknown>|4|False|GO AWAY!!!
<unknown>|5|False|GO AWAY!!!
<unknown>|6|False|C R E E E E E! !!
<unknown>|7|False|Ooutch!
<unknown>|8|False|He! That's... n-NOT... outch... nice!
<unknown>|9|False|Hmm!
<unknown>|10|False|Grrr!
<unknown>|11|False|...You will have deserved it!
<unknown>|12|False|B R Z OO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|CURSUS CANCELARE MAXIMUS !!!
<unknown>|2|False|S H K L AK!
<unknown>|3|False|Outch!...
<unknown>|4|False|P AF !!
<unknown>|5|False|Even your maximum cancellation spell had no effect on me!
<unknown>|6|False|Give up Pepper , and go away!
<unknown>|7|False|Do not make me hurt you more!
<unknown>|8|False|Oh, my spell worked out really well, except you weren't the target.
<unknown>|9|False|What do you mean?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|?!!
<unknown>|2|False|My target was her!
<unknown>|3|False|I just canceled her spell which keeps her looking like a young woman when she is no longer the case!
<unknown>|4|False|I immediately saw this detail when I arrived.
<unknown>|5|False|it's perhaps not much, but THIS is for forcing Shichimi to fight against me!
<unknown>|6|False|INSOLANT!
<unknown>|7|False|How dare you ?!
<unknown>|8|False|in front of my whole school!
<unknown>|9|False|Consider yourself lucky!
<unknown>|10|False|I would have had all my energy, I would have gone much further.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|DR Z OW!!
<unknown>|2|False|I see, you seem to have arrived at the level of your predecessors a little earlier than I expected....
<unknown>|3|False|This precipitates my plans a bit but it's a good thing!
<unknown>|4|False|Your plans?...
<unknown>|5|False|So I understood correctly; you were testing me and it had nothing to do with my delay...
<unknown>|6|False|You are really twisted!
<unknown>|7|False|tee...
<unknown>|8|False|hee.
<unknown>|9|False|WHAT ARE YOU ALL STARING AT?!!
<unknown>|10|False|I JUST GOT ATTACKED AND YOU ARE ALL JUST STANDING HERE ?!! CATCH HER!!!
<unknown>|11|False|I WANT HER ALIVE!
<unknown>|12|False|CATCH-HER!!!
<unknown>|13|False|Shichimi, we will talk about this later!
<unknown>|14|False|Carrot, hold on tight, I'm sorry but we have no choice but to take off at high speed ...
<unknown>|15|False|Tap!
<unknown>|16|False|Tap!
<unknown>|17|False|Ziioo OO!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|? !
<unknown>|2|False|CATCH-HER!!!
<unknown>|3|False|...Oh dear.
<unknown>|4|False|Pepper, take my broom!
<unknown>|5|False|Fizzz!
<unknown>|6|False|...Oh wow! Thank you Safron!
<unknown>|7|False|Toc!
<unknown>|8|False|zioo O O O O!!
<unknown>|9|False|TO BE CONTINUED...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<unknown>|1|False|You too can become a patron of Pepper&Carrot and get your name here!
<unknown>|2|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
<unknown>|3|False|For this episode, thanks go to %%%% patrons!
<unknown>|4|False|Check www.peppercarrot.com for more info!
<unknown>|5|False|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
<unknown>|6|False|Thank you!
<unknown>|7|False|Did you know?
<unknown>|8|False|March xx, 2021 Art & scenario: David Revoy. Beta readers: (to update) Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. English version (original version) Proofreading: (to update) Craig Maloney, GunChleoc, Karl Ove Hufthammer, Martin Disch. . Special thanks: to Nartance for exploring Wasabi character in his fan-fiction novels. The way he imagined her for sure had a big impact on my way to depict her in this episode. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.4.1, Inkscape 1.0.2 on Kubuntu Linux 20.04. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<unknown>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<unknown>|10|False|You can also translate this page if you want.
<unknown>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
