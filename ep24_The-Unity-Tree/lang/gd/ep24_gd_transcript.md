# Transcript of Pepper&Carrot Episode 24 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 24: Craobh an Aonaidh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|5|False|DÒRN !|nowhitespace
Cìob|1|True|... ’s tusa ag iarraidh Craobh Aonaidh!
Cìob|4|False|’S e bana-bhuidsichean an dubh-choimeisg a th’ annainn ’s CHA DÈAN sinn a leithid rud.
Cìob|6|False|B’ fheàirrde thu d’ aire a chur air “Geasan millidh”!
Peabar|7|True|Milleadh ann an da-rìbibh?
Peabar|8|True|An ràith seo?
Peabar|9|False|Carson nach ionnsaichinn geasan cruthachaidh ’nan àite?
Cìob|10|False|Tha ath Chomhairle nan Trì Gealaichean gu bhith tighinn oirnn mar a tha na deuchainnean agad. Ionnsaich! Ionnsaich! IONNSAICH!!! AGUS SEACHAIN AM BUAIREADH A CHUIREAS DO BHEACHDAN AMAIDEACH ORT!!!
Cìob|2|False|Ann a sheo!!!
Cìob|3|True|CHAN FHAIGH!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Cl ab !|nowhitespace
Peabar|5|True|Geamhradh gun Chraobh Aonaidh?!
Peabar|7|False|Saoil an cruthaichinn craobh leam fhìn le buidseachas?!
Peabar|8|True|Smaoinich! B’ urrainn dhomh fiù ’s feuchainn gun cuirinn buidseachas na Clann-fhlùrachd ’s na Dubh-choimeasgachd ri chèile!
Peabar|9|True|Craobh mhòr uaine le solas reultan ’s rianan-grèine bìodach oirre!
Peabar|10|False|Àgh! Bhiodh i foirfe!
Peabar|11|True|’S bhiodh i
Peabar|12|False|cruthachail!
Peabar|13|True|Ach chan eil dòigh san dèanainn an seòrsa seo dhe bhuidseachas ann a sheo...
Peabar|14|False|... mhothaicheadh na bana-ghoistidhean air sa bhad mura robh mi ri “Geasan millidh”.
Curran|3|False|gnogag
Fuaim|2|False|Ffff
Peabar|6|False|Chan fhulaing!
Peabar|4|False|Grrrrr...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|3|False|Fuiss
Fuaim|17|False|SiiING
Sgrìobhte|22|False|Am Pàipear Mòr
Peabar|1|True|Ach...
Peabar|2|False|’Eil chuimhn’ a’m air...
Peabar|4|False|Sin e! Sa chaibideal “Geasan millidh”, tha Tìom a’ moladh gum feuchte riutha ann am meanbh-chruinne. Ach seall air a’ phailteas dhe rabhaidhean seo:
Peabar|16|False|Siuthadamaid ma-thà!
Carabhaidh|19|False|An e siud Peabar a’ cruthachadh meanbh-chruinne?
Fuaim|18|False|SIUUuuump! !|nowhitespace
Tìom|20|True|Glè mhath!
Sgrìobhte|8|False|“ NA DOIR dad air ais leat air adhbhar tèarainteachd.”
Sgrìobhte|6|False|“ NA CAITH do Rèatha uile gu lèir le cruthachadh a’ mheanbh-chruinne.”
Sgrìobhte|11|True|“ NA BI AN DÙIL air taic sam bith o thaobh a-muigh a’ mheanbh-chruinne...
Peabar|7|False|Gu dearbh.
Peabar|10|False|Seo dhut a-nise!
Peabar|9|False|Na can rium!
Peabar|5|False|Thà...
Sgrìobhte|12|False|...chan fhaic duine sam bith dè bhios tu ris ! ”
Tìom|21|False|Tha mi ’n dòchas gun do thuig i na rabhaidhean uile a sgrìobh mi sa chaibideal mu na “Geasan millidh”.
Peabar|15|True|Tha sin foirfe!
Peabar|13|True|Nach fhaic?
Peabar|14|True|Duine sam bith!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Sgoinneil!
Peabar|2|True|Tha e mar shoitheach ’s chan eil ’na bhroinn ach a’ chuid dhen leabhar-lann a dh’fheumas mi. Agus tha e glan sgaraichte on fhìorachd againn fhèin.
Peabar|3|False|Seo bogsa-gainmhich foirfe a chùm deuchainn air geasan.
Peabar|4|False|Tòisichidh mi air an annas-làimhe agam ma-thà!
Fuaim|5|False|SiiING
Fuaim|6|False|PLUuuum! !|nowhitespace
Fuaim|8|False|Daoobh! !|nowhitespace
Fuaim|10|False|Chding! !|nowhitespace
Peabar|7|False|Hè. Iochd.
Peabar|9|False|Ceach!
Peabar|11|False|Seo i! Tha i rìomhach ach ro bheag...
Peabar|13|False|CUT, A CHURRAIN! NA BEAN RITHE!
Curran|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Pluuum! !|nowhitespace
Peabar|3|True|Cha chreid mi gun dèanadh e cron nan doirinn a’ chraobh seo leam às a’ mheanbh-chruinne.
Peabar|2|True|Seo dhut! Mheudaich mi i. Tha coltas foirfe oirre a-nis!
Peabar|5|True|Ceart ma-thà.
Peabar|6|True|Cuireamaid am meanbh-chruinne seo dhan fhìorachd againne ’s seallaidh sinn dhaibh dè cho cruthachail ’s a dh’fhaodas an Dubh-choimeasg a bhith!
Fuaim|8|False|DOooong! !|nowhitespace
Tìom|9|True|An-dà... Seo Peabar air a tilleadh.
Tìom|10|False|Meal do naidheachd, a Chìob. Tha dòighean an fhoghlaim agad torrach mu dheireadh thall.
Cìob|11|False|Mòran taing, a bhana-mhaighstir.
Peabar|7|False|Tha fiughair agam ri fiamhan an aodannan!
Sgrìobhte|12|False|Am Pàipear Mòr
Peabar|4|False|Chan e ach Craobh Aonaidh a th’ innte ’s eu-coltach ri “Geas millidh”.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|5|True|BRRrrr
Fuaim|6|True|BRRr
Fuaim|7|True|BRRrrr
Fuaim|8|True|BRRrrr
Fuaim|9|True|BRRrrr
Fuaim|10|False|BRrr
Curran|11|False|?
Curran|13|False|!!
Fuaim|14|True|BRRr
Fuaim|15|False|BRRrrrr
Curran|16|False|!!
Peabar|1|True|A CHURRAIN!
Peabar|2|True|Bidh mi air ais an ceann greiseig le Tìom, Cìob is Carabhaidh.
Peabar|3|False|Na bean ris a’ chraobh fhad ’s a bhios mi shìos, ceart gu leòr?
Peabar|4|False|Bidh mi air ais a dh’aithghearr!
Peabar|17|True|Ullaichibh dhan annas is iongantas! Chan fhaca sibh a leithid a-riamh.
Peabar|18|False|Leanaibh orm suas an staidhre ach am faic sibh i...
Fuaim|12|False|Chruuuf!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|6|False|- Deireadh na sgeòil -
Fuaim|1|False|Chruuuuf!!
Fuaim|4|False|BRAG! !!|nowhitespace
Fuaim|3|False|Fuuum! !!|nowhitespace
Peabar|2|False|?!!
Peabar|5|False|Feumaidh gun do rinn mi ath-sgrùdadh air na “Geasan millidh” ge b’ oil leam!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|2|False|12/2017 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|3|False|Piseach air na còmhraidhean: Craig Maloney, CalimeroTeknik, Jookia.
Urram|5|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|6|False|Bathar-bog: Krita 3.2.3, Inkscape 0.92.2 air Ubuntu 17.10
Urram|7|False|Ceadachas: Creative Commons Attribution 4.0
Urram|9|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
Urram|8|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 810 pàtran a thug taic dhan eapasod seo:
Urram|4|False|Ceartachadh/Beachdan Beta: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin ’s xHire.
Urram|1|False|Nollaig chridheil dhuibh!
